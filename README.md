# IAC Toolbox

Creates a Docker image for an IAC toolbox used to manage AWS infrastructure with Terraform, Ansible, and various other tools.