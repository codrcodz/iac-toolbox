# CONTRIBUTING

This repository is for personal use only at the moment. I have no plans to add testing, version tagging, or anything else fancy to it. That being said, if you find it useful and wish to submit a PR, those are always welcome.
